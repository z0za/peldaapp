FROM golang:1.17 AS build

WORKDIR /app

COPY go.mod ./
RUN go mod download

COPY *.go ./

RUN go build -o /PeldaApp

##
FROM ubuntu

WORKDIR /

COPY --from=build /PeldaApp /PeldaApp

EXPOSE 8080

ENTRYPOINT ["/PeldaApp"]